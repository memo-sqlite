import Control.Monad (liftM2)
import System.Environment (getArgs)
import qualified Data.Text as T
import Data.Memo.Sqlite (memoRec', readShow, table)

-- | Compute Fibonacci numbers.
fib :: (Integer -> IO Integer) -> Integer -> IO Integer
fib _fib' n@0 = print n >> return 0
fib _fib' n@1 = print n >> return 1
fib  fib' n   = print n >> liftM2 (+) (fib' (n - 1)) (fib' (n - 2))

-- | Example program.
main :: IO ()
main = do
  [file, ts, ns] <- getArgs
  let Just t = table (T.pack ts)
      n = read ns
  (cleanup, fib') <- memoRec' readShow (T.pack file) t fib
  fib' n >>= \fn -> putStrLn $ "fib(" ++ show n ++ ") = " ++ show fn
  cleanup

{-
  Example usage:

  $ ghc -O2 -Wall --make fib.hs 
  $ ./fib fibs.sqlite3 fibs 10
  $ ./fib fibs.sqlite3 fibs 10
  $ ./fib fibs.sqlite3 fibs 100
  $ ./fib fibs.sqlite3 fibs 100
-}
